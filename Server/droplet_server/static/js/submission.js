//first param is if the text was valid or not
//second param is the input element of the text
//third param is the element for the error alert
function text_verification(valid, element, error_element)
{
	if (!valid)
	{
		//$(element).addClass('placeholderred');
		//(element).css("borderColor", "#dd1e35");
		$(error_element).css("display", "block");
		$(element).addClass("bad_input"); //defined in submission.css
	}
	else
	{
		$(element).removeClass("bad_input"); //defined in submission.css
		$(error_element).css("display", "none");	
	}
}