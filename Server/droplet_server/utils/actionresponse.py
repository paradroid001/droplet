import json

class ActionResponse(object):
    
    def __init__(self):
        self.success = False;
        self.errorstr = ""
        self.datastr = ""
        self.userstr = ""

    def to_json(self):
        return json.dumps(self.__dict__)



