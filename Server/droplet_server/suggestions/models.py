# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.db import models
from django.core.exceptions import ValidationError

class SuggestionSettings(models.Model):
    #methods of selecting data
    LATEST = 'LAT'
    RANDOM = 'RND'
    VOTES = 'VOT'
    QUESTION_SELECTION_OPTIONS = (
        (LATEST, 'Latest entries'),
        (RANDOM, 'Random entries')
    )
    ORB_SELECTION_OPTIONS = (
        (LATEST, 'Latest entries'),
        (RANDOM, 'Random entries'),
        (VOTES, 'Highest votes')
    )

    profanity = models.TextField(blank=True, help_text="A space separated list of words to check for on user editable input text.")
    questions_shown = models.SmallIntegerField(default=3, help_text="How many questions should users be shown")
    question_selection = models.CharField(max_length=3, choices=QUESTION_SELECTION_OPTIONS, default = RANDOM)
    suggestion_soft_limit = models.IntegerField(default = 255, help_text="How many characters should be allowed in suggestions? (max 2048)")
    
    client_orb_limit = models.SmallIntegerField(default=1000, help_text="Maximum number of orbs to display on the client side. 0 = unlimited")
    client_orb_selection = models.CharField(max_length=3, choices=ORB_SELECTION_OPTIONS, default = RANDOM)
    client_globe_max_size = models.FloatField(default=400.0)
    client_globe_min_size = models.FloatField(default=300.0)
    #how often do we check for new data?
    client_data_update_frequency = models.IntegerField(default=30) #30 second updates
    client_zoomed_out_colour = models.CharField(max_length=7, default="#0000FF")
    client_zoomed_in_colour = models.CharField(max_length=7, default="#0000FF")
    client_control_sensitivity_h = models.FloatField(default=1.0)
    client_control_sensitivity_v = models.FloatField(default=1.0)
    client_control_sensitivity_z = models.FloatField(default=1.0)

    class Meta:
        verbose_name = "Suggestion Settings"
        verbose_name_plural = "Suggestion Settings"
    def prepare(self):
        d = {}
        d["questions_shown"] = self.questions_shown
        d["question_selection"] = self.question_selection
        d["suggestion_soft_limit"] = self.suggestion_soft_limit
        
        d["client_orb_limit"] = self.client_orb_limit
        d["client_orb_selection"] = self.client_orb_selection
        d["client_globe_max_size"] = self.client_globe_max_size
        d["client_globe_min_size"] = self.client_globe_min_size
        d["client_data_update_frequency"] = self.client_data_update_frequency 
        d["client_zoomed_out_colour"] = self.client_zoomed_out_colour
        d["client_zoomed_in_colour"] = self.client_zoomed_in_colour
        d["client_control_sensitivity_h"] = self.client_control_sensitivity_h
        d["client_control_sensitivity_v"] = self.client_control_sensitivity_v
        d["client_control_sensitivity_z"] = self.client_control_sensitivity_z
        
        return d

    @classmethod
    def instance(*args):
        return SuggestionSettings.objects.get(pk=1)

    def save(self, *args, **kwargs):
        if SuggestionSettings.objects.exists() and not self.pk:
        # if you'll not check for self.pk 
        # then error will also raised in update of exists model
            raise ValidationError('There is can be only one Settings instance')
        return super(SuggestionSettings, self).save(*args, **kwargs)

class SuggestorDetails(models.Model):
    name = models.CharField(max_length = 100, null=True, blank=True, default ='Anonymous')
    email = models.CharField(max_length=100, null=True, blank=True)
    
    class Meta:
        verbose_name = "Suggestor Details"
        verbose_name_plural = "Suggestor Details"

    def __unicode__(self):
        return self.name

    def as_dict(self):
        ret = {}
        ret["name"] = self.name
        return ret

class SuggestionCategory(models.Model):
    name = models.CharField(max_length=50, help_text="Category Name")
    enabled = models.BooleanField(default=True, help_text="If checked, suggestions from this category are shown in Globe View")
    colour1 = models.CharField(max_length=7, default="#0000FF")
    colour2 = models.CharField(max_length=7, default="#0000FF")
    colour3 = models.CharField(max_length=7, default="#0000FF")
    colour4 = models.CharField(max_length=7, default="#0000FF")
    colour5 = models.CharField(max_length=7, default="#0000FF")
    image = models.CharField(max_length=255, help_text="Icon Image: 256x256px")
    last_modified_date = models.DateTimeField('modified', auto_now=True)

    class Meta:
        verbose_name = "Suggestion Category"
        verbose_name_plural = "Suggestion Categories"

    def __unicode__(self):
        return "%s" % self.name

    def color(self):
        return random.choice([self.colour1, self.colour2, self.colour3, self.colour4, self.colour5])

    def prepare(self):
        d = {}
        d["name"] = self.name
        d["enabled"] = self.enabled
        d["colours"] = [self.colour1, self.colour2, self.colour3, self.colour4, self.colour5]
        d["image"] = self.image
        d["last_modified_date"] = str(self.last_modified_date)
        return d

class Question(models.Model):
    title = models.CharField(max_length = 255, default="Enter Question Text Here", help_text="Text of the question")
    enabled = models.BooleanField(default=True, help_text="If checked, answers to this question are shown in Globe View")
    allow_submissions = models.BooleanField(default=True, help_text="If checked, users can submit answers to this question.")
    category = models.ForeignKey(SuggestionCategory)
    created = models.DateTimeField('modified', auto_now_add=True) 
    def __unicode__(self):
        return "[%s] %s" % (self.category, self.title)


class SuggestionStyle(models.Model):
    name = models.CharField(max_length=50, help_text="The name of the style in the suggestion app")
    fontname = models.CharField(max_length=50, help_text="The name of the font")
    enabled = models.BooleanField(default=True, help_text="If enabled, this style will be available in the suggestion app")

    class Meta:
        verbose_name = "Suggestor Style"
        verbose_name_plural = "Suggestor Styles"

    def __unicode__(self):
        return "%s (%s)" % (self.name, "Enabled" if self.enabled else "Disabled")

    def fontstr(self):
        if (self.enabled):
            return self.fontname
        else:
            return "Times New Roman"



# Create your models here.
class Suggestion(models.Model):
    text = models.CharField(max_length=2048) #hard limit on suggestions. Soft limit in settings
    last_modified_date = models.DateTimeField('modified', auto_now=True)
    #category = models.ForeignKey(SuggestionCategory)
    question = models.ForeignKey(Question)
    suggestor = models.ForeignKey(SuggestorDetails, null=True) #there might be no named suggestor
    style = models.ForeignKey(SuggestionStyle, null=True)
    enabled = models.BooleanField(default=True)
    votes = models.IntegerField(default=0)

    def prepare(self):
        d = {}
        d["id"] = self.id
        d["text"] = self.text
        d["question"] = "%s" % self.question.title
        d["category"] = "%s" % self.question.category
        d["enabled"] = self.enabled
        d["style"] = self.style.fontstr() #.as_dict()
        if self.suggestor is not None:
            d["suggestor"] = self.suggestor.__unicode__() #.as_dict()
        d["last_modified_date"] = str(self.last_modified_date)
        d["votes"] = self.votes
        return d

    def get_font(self):
        if (self.style != None):
            return self.style.fontname
        else:
            return "Arial"
    
    def add_vode(self):
        self.votes += 1
        self.save()

    def to_json(self):
        return json.dumps(self.prepare)

    def __unicode__(self):
        return "[%s] %s" % (self.question.category, self.text)


