from django.forms import ModelForm, CharField
from django.forms.widgets import TextInput
from django.shortcuts import render
from .models import SuggestionCategory

class SuggestionCategoryForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(SuggestionCategoryForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            print field
            if field.startswith('colour'):
                self.fields[field].widget = TextInput(attrs={
                    'type' : 'color',
                })

