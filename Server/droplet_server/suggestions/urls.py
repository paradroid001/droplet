from django.conf.urls import url
from django.contrib import admin

from views import suggestions, accepted, suggest, \
                  profanity_check, similarity_check, \
                  privacy, categories, vote, settings, \
                  newestSuggestion, randomSuggestion, popularSuggestion

urlpatterns = [
    url(r'^suggest/', suggest),
	url(r'^accepted/', accepted),
    url(r'^profanity/', profanity_check),
    url(r'^similarity/', similarity_check),
    url(r'^all/', suggestions),
    url(r'^categories', categories),
    url(r'^privacy/', privacy),
    url(r'^settings/', settings),
    url(r'^vote/([0-9]+)/$', vote),
    url(r'^newest/$', newestSuggestion),
    url(r'^random/$', randomSuggestion),
    url(r'^popular/$', popularSuggestion)
]
