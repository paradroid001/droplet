# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.conf import settings as django_settings

from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, JsonResponse, Http404
from django.http import HttpResponseRedirect
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt

from suggestions.models import *
from utils import ActionResponse
import random
import os


# Create your views here.
@xframe_options_exempt
@csrf_exempt
def profanity_check(request):
    if request.method == 'POST':
        text = request.POST.get('text')
    
    return HttpResponse(json.dumps(check_clean_text(text) ) )

@xframe_options_exempt
@csrf_exempt
def similarity_check(request):
    if request.method == 'POST':
        print request.POST
    return HttpResponse(json.dumps(True))

#false = profanity
#true = ok
def check_clean_text(text):
    result = True
    if text is None:
        result = False
    else:
        text = text.lower()
        banned_words = [word.strip() for word in SuggestionSettings.instance().profanity.split('\n') ]
        if any(word in text for word in banned_words):
            result=False
        else:
            #We passed.
            result = True
    return result

@xframe_options_exempt
def suggest(request):
    if request.method == 'POST':
        try:
            questionid = request.POST['questionid']
            submitter_name = request.POST['submitter_name']
            styleid = request.POST['styleid']
            answertext = request.POST['answer_text']
            validSuggestion = True
            validName = True
            
			
            person = None
            
			#test and save the person so we can reference their name later
            if submitter_name is not "":
                validName = check_clean_text(submitter_name)
                if validName:
                    person = SuggestorDetails()
                    person.name = submitter_name
                    person.save()
            
            #create a new suggestion from the submitted data and save it.
            validSuggestion = check_clean_text(answertext)
            if validSuggestion:
                s = Suggestion()
                s.text = answertext
                s.question = Question.objects.get(id=questionid)
                if person is not None:
                    s.suggestor = person
                s.style = SuggestionStyle.objects.get(id=styleid)
            
            if not validSuggestion or not validName:
                raise Exception("Invalid Text")
            
            #render the 'thanks' page
            s.save()	
            template = loader.get_template('accepted.html')
            return HttpResponse(template.render(None, request) )

        except Exception, e:
            #error interaction here
            template = loader.get_template('submission_error.html')
            return HttpResponse(template.render(None, request) )

    # When GET request...
    else:
        settings = SuggestionSettings.instance()
        questionNum = settings.questions_shown
        questionSort = settings.question_selection
        
        if questionSort == "RND":
            questions = [q for q in Question.objects.filter(allow_submissions=True)]
            random.shuffle(questions)
            questions = questions[0:questionNum]
        elif questionSort == "LAT":
            questions = Question.objects.filter(allow_submissions=True).order_by('-created') [0:questionNum]
        else:
            return HttpResponse("Error: Questions have not loaded in properly please contact staff to resolve this issue")            

        styles = [s for s in SuggestionStyle.objects.all() if s.enabled]
        template = loader.get_template('suggest.html')
        context = { 
                'stylesheet': "suggest.css",
                'page_title':"Make A Suggestion",
                'content_text': "",
                'styles' : styles,
                'fonts': ['Arial', 'Helvetica', 'Courier'],
                'questions' : questions,
                }
        return HttpResponse(template.render(context, request) )
		
@xframe_options_exempt
def index(request):
    if request.method == 'GET':
        links = {}
        links["suggestions_interface"] = "/suggestion/suggest/"
        links["admin"] = "/admin/"
        #links["tablet_remote_interface"] = "/static/builds/current/tablet/interface/"
        #links["tablet_suggestion_interface"] = "/static/builds/current/tablet/suggestion/"
        vis_list =os.listdir(os.path.join(django_settings.BASE_DIR,  "static/builds/current/vis/") )
        remote_list = os.listdir(os.path.join(django_settings.BASE_DIR, "static/builds/current/tablet/interface/") )
        suggestion_list = os.listdir(os.path.join(django_settings.BASE_DIR, "static/builds/current/tablet/suggestion/") )


        try:
            template = loader.get_template('index.html')
            return HttpResponse(template.render({
                'links': links, 
                'vis_list': vis_list,
                'remote_list': remote_list,
                'suggestion_list': suggestion_list }, request))

        except Exception, e:
                return HttpResponse("There has been an error with the server please alert a member of staff" % e)

@xframe_options_exempt
def privacy(request):
    if request.method == 'GET':
        try:
            template = loader.get_template('privacy.html')
            return HttpResponse(template.render(None, request))

        except Exception, e:
                return HttpResponse("There has been an error with the server please alert a member of staff" % e)

@xframe_options_exempt
def accepted(request):
    if request.method == 'GET':
        try:
            #render the 'suggest' page
            template = loader.get_template('suggest.html')
            return HttpResponse(template.render(None, request))

        except Exception, e:
            return HttpResponse("There has been an error with the server please alert a member of staff" % e)

@xframe_options_exempt			
def suggestions(request):

    #get all suggestions
    suggestions = Suggestion.objects.all().order_by("last_modified_date")
    
    #print suggestions
    #return HttpResponse(json.dumps(list(suggestions), indent=2))
    
    #data = serializers.serialize('json', suggestions, fields=('text', 'enabled', 'style', 
    #'suggestor', 'last_modified_date',) )

    data = [f.prepare() for f in suggestions]
    #data = suggestions[0].to_json()
    
    ar = ActionResponse()
    ar.datastr = json.dumps({"suggestions" : data})
    ar.success = True
    return HttpResponse(ar.to_json() )

def categories(request):
    #get all categories
    categories = SuggestionCategory.objects.all();
    data = [f.prepare() for f in categories]
    ar = ActionResponse()
    ar.datastr = json.dumps({"categories" : data})
    ar.success = True
    return HttpResponse(ar.to_json() )

#add one vote
def vote(request, id):
    ar = ActionResponse()
    try:
        suggestion = Suggestion.objects.get(pk=id)
        suggestion.votes += 1
        suggestion.save(update_fields=['votes']) #avoid last modified time.
        ar.datastr = json.dumps(suggestion.prepare() )
        ar.success = True
    except Suggestion.DoesNotExist, e:
        ar.errorstr = json.dumps("exception: %s" % (str(e) ) )
        ar.success = False
    return HttpResponse(ar.to_json() )

def newestSuggestion(request):
    ar = ActionResponse()
    try:
        suggestions = Suggestion.objects.all().order_by('-last_modified_date')
        suggestion = suggestions[0]
        ar.datastr = json.dumps(suggestion.prepare() )
        ar.success = True
    except Suggestion.DoesNotExist, e:
        ar.errorstr = json.dumps("exception: %s" % (str(e) ) )
        ar.success = False
    return HttpResponse(ar.to_json() )

def randomSuggestion(request):
    ar = ActionResponse()
    try:
        suggestions = Suggestion.objects.all()
        suggestion = random.choice(suggestions)
        ar.datastr = json.dumps(suggestion.prepare() )
        ar.success = True
    except Suggestion.DoesNotExist, e:
        ar.errorstr = json.dumps("exception: %s" % (str(e) ) )
        ar.success = False
    return HttpResponse(ar.to_json() )

def popularSuggestion(request):
    ar = ActionResponse()
    try:
        suggestions = Suggestion.objects.all().order_by("-votes")
        suggestion = suggestions[0]
        ar.datastr = json.dumps(suggestion.prepare() )
        ar.success = True
    except Suggestion.DoesNotExist, e:
        ar.errorstr = json.dumps("exception: %s" % (str(e) ) )
        ar.success = False
    return HttpResponse(ar.to_json() )



def settings(request):
    #there's only one suggestion settings object
    s = SuggestionSettings.instance()
    ar = ActionResponse()
    ar.datastr = json.dumps(s.prepare())
    ar.success = True
    return HttpResponse(ar.to_json() )
