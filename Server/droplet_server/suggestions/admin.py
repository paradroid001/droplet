# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from suggestions.models import *
# Register your models here.
admin.site.register(SuggestionSettings)
admin.site.register(SuggestorDetails)
admin.site.register(Suggestion)
admin.site.register(Question)
admin.site.register(SuggestionStyle)
from .forms import SuggestionCategoryForm

@admin.register(SuggestionCategory)
class SuggestionCategoryAdmin(admin.ModelAdmin):
    form = SuggestionCategoryForm
    #filter_horizontal = ('questions',)
    fieldsets = (
        (None, {
            'fields': ( ('name',
                        'enabled'),
                        #'image',), 
                        ('colour1',
                         'colour2',
                         'colour3',
                         'colour4',
                         'colour5')
                )
            }),
        )
