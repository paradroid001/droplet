from blueprint import VirtualConfig
###
# This is a template for a config.py to be used with blueprint
# Useful VirtualConfig operations:
# blah = VirtualConfig(configname, base=config_to_derive_from)
# .deps_root = root dir where dependencies are stored
# .deps_dir = dir relative to root to find deps. Default is configname, unless overridden ('' for none)
# .deps_motd_file = file in deps_dir which contains important text to display during install (default DEPENDENCIES)
# .deps_env_file = file in deps dir which contains env to be injected into your virtualenv (default ENVIRONMENT)
# .add_local(common name, file name), 
# .override_local(common name) : adds a local file to the deps to install, keyed by the given 'common name'
# .add_remote(common name, package name) : adds a remote package to fetch via pip, keyed on given 'common name'
# .remove_local(common name), .remove_remote(common_name) : remove a named local/remote dependency
###


### Example base config ###
### Just the specific deps, all located in the eggs dir
baseconfig = VirtualConfig('baseconfig')  #Begin a new config, call it baseconfig
baseconfig.deps_root = 'eggcache'             #Set the root deps dir, for self and all derivatives
baseconfig.deps_dir = ''                  #Deps dir relative to root - empty has no effect



webconfig = VirtualConfig('droplet', base=baseconfig)
webconfig.deps_dir = ''                  #Deps dir relative to root - empty has no effect
webconfig.add_local('django',              'Django-1.11.4.tar.gz')     #add dep
#webconfig.add_local('dmp',              'django-mako-plus-master.zip')     #add dep
#webconfig.add_local('mako',              'Mako-1.0.1.tar.gz')     #add dep

### Make all three configs available via the 
#Config keys are the names used by the -c flag
CONFIGS = [ baseconfig, webconfig ] 
