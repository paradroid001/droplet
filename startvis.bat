@echo off
SET rootdir=c:\Droplet\droplet
SET serverdir=Server
SET builddir=droplet_server\static\builds\current\vis\droplet

SET visdir=cd /d %rootdir%\%serverdir%\%builddir%
SET vis=droplet.exe
@echo on
cmd /k "%visdir% & %vis% & exit"
