@echo off
SET rootdir=c:\Droplet\droplet

SET update=git pull
SET gitdir=cd /d %rootdir%

@echo on
cmd /k "%gitdir% & %update% & pause & exit"
