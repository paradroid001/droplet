import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  data:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
   /*
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
    */
    /*
    this.storage.forEach((value, key, iterationNumber) =>
    {
      console.log(key + ": " + value);
    })
    */
    this.data = "Enter URL Here";
    this.storage.get('url').then((val) =>
    {
      this.data = val;
    });
  }

  setClicked(event, item)
  {
    console.log("Data was "+ this.data);
    //we use trust html because this has come from the page.
    
    //var url = this.sanitize.bypassSecurityTrustHtml(this.data);
    //console.log("Clicked: " + url.toString());
    this.storage.set('url', this.data);
  }  
}
