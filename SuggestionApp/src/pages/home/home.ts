
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';
import { Platform } from 'ionic-angular';
import { InAppBrowser , InAppBrowserOptions } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage 
{
  url: any;
  options : InAppBrowserOptions = {
    location : 'no',//Or 'no' 
    hideurlbar: 'yes',
    //hidden : 'no', //Or  'yes'
    //clearcache : 'yes',
    //clearsessioncache : 'yes',
    zoom : 'no',//Android only ,shows browser zoom controls 
    hardwareback : 'no',
    //mediaPlaybackRequiresUserAction : 'no',
    //shouldPauseOnSuspend : 'no', //Android only 
    //closebuttoncaption : 'Close', //iOS only
    //disallowoverscroll : 'no', //iOS only 
    toolbar : 'no', //iOS only 
    //enableViewportScale : 'no', //iOS only 
    //allowInlineMediaPlayback : 'no',//iOS only 
    //presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
  };
  constructor(public navCtrl: NavController, public platform: Platform, 
              private sanitize: DomSanitizer, private storage: Storage, 
              private iab:InAppBrowser) 
  {
    this.platform.ready().then(() => 
    {
      this.url = this.getURL();
      
      //browser.show();
    });

    /*
    var urlData = navParams.get('data');
    console.log(urlData);
    if (urlData == null)
    {
      console.log("was undefined");
      this.url = "";
    }
    else
    {
      console.log("wasn't undefined. Setting to " + urlData);
      this.url = urlData;
    }
    */
    //let nav = document.getElementsByTagName("ion-navbar")[0];
    //nav.style.display = "none";
    
  }

  getURL()
  {
    console.log("getURL was called");
    var urlvalue = "Nothing";
    //return this.sanitize.bypassSecurityTrustResourceUrl("http://www.murdoch.edu.au/");
    // Or to get a key/value pair
    this.storage.get('url').then( (val) => 
    {
      urlvalue = val;
      console.log("url existed and is " + urlvalue);
      this.url = urlvalue;
    });
  }

  getSafeURL()
  {
    return this.sanitize.bypassSecurityTrustResourceUrl(this.url);
  }

  start()
  {
      console.log("Opening browser to " + this.url);
      const browser = this.iab.create(this.url,'_blank', this.options); 
  }

}