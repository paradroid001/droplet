@echo off
SET rootdir=c:\Droplet\droplet
SET serverdir=Server
SET listenip=0.0.0.0:8000

SET vedir=cd /d %rootdir%\%serverdir%\ve_droplet\Scripts
SET ve=activate
SET servedir=cd /d %rootdir%\%serverdir%\droplet_server
SET serve=python manage.py runserver %listenip%
SET browse=start chrome http://127.0.0.1:8000/admin
@echo on
cmd /k "%browse% & %vedir% & %ve% & %servedir% & %serve% & exit"
