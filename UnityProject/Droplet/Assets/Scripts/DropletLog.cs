﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropletLog : MonoBehaviour 
{
	public static DropletLog _instance;

	public GameObject errorMessagePrefab;
	public int maxMessages = 10;
	private int numMessages;

	void Awake()
	{
		_instance = this;
	}

	// Use this for initialization
	void Start () 
	{
		numMessages = 0;	
	}
	
	// Update is	called once per frame
	void Update () {
		
	}

	public static void Log(string title, string msg)
	{
		_instance.AddLog(title, msg);
	}

	public void AddLog(string title, string msg)
	{
		if (numMessages >= maxMessages)
		{
			//remove first child.

		}

		GameObject newmsg = Instantiate(errorMessagePrefab);
		newmsg.GetComponent<Text>().text = "<color=red>" + title + "</color>: " + msg;
		//remove oldest message
		if (transform.childCount >= maxMessages)
		{
			Transform topChild = transform.GetChild(0);
			topChild.SetParent(null);
			Destroy( topChild.gameObject );
		}
		
		newmsg.transform.SetParent(transform);
		newmsg.transform.SetAsLastSibling();
	}
}
