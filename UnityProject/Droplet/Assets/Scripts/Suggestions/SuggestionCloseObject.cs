﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuggestionCloseObject : MonoBehaviour {

	public SuggestionObject suggestionObject;
	[Range(0.0f, 1.0f)]
	public float alpha = 0.2f;
	Material _mat;

	void Awake()
	{
		_mat = GetComponent<Renderer>().materials[0];
		
	}

	// Use this for initialization
	void Start () {
		Color c = suggestionObject.categoryColour;
		c.a = alpha;
		_mat.color = c;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
