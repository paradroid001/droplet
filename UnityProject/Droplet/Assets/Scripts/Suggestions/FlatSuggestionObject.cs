﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatSuggestionObject : MonoBehaviour 
{
	public SuggestionObject suggestionObject;
	Material _mat;


	
	void Awake()
	{
		_mat = GetComponent<Renderer>().materials[0];
	}
	// Use this for initialization
	void Start () 
	{
		Color c = suggestionObject.categoryColour;
		_mat.SetColor ("_EmissionColor", c);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
