﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour 
{

    public bool billBoard;
    public float distanceThreshold = 200.0f;
    public GameObject closeObject;
    public GameObject farObject;
    private bool revealed = true;

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (billBoard)
            transform.LookAt(Camera.main.transform.position, Vector3.up);

        float dist = Vector3.Distance(Camera.main.gameObject.transform.position, transform.position);
        
        if (dist <= distanceThreshold)
        {
            if (!revealed)
            {
                revealed = true;
                closeObject.SetActive(true);
                farObject.SetActive(false);
                //transform.GetChild(0).gameObject.SetActive(true);
                //transform.GetChild(1).gameObject.SetActive(false);
                GetComponent<SuggestionObject>().OnReveal();
            }
        }
        else
        {
            if (revealed)
            {
                revealed = false;
                //transform.GetChild(1).gameObject.SetActive(true);
                //transform.GetChild(0).gameObject.SetActive(false);
                closeObject.SetActive(false);
                farObject.SetActive(true);
                //GetComponent<SuggestionObject>().OnHide();
            }
        }
	}
}
