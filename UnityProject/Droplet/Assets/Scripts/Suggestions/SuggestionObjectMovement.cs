﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Base class for suggestionobject movement
public class SuggestionObjectMovement : MonoBehaviour 
{
    protected SuggestionObject _sobject;
    public float billboardDistance = 10f;
    public bool faceCamera = true;

	// Use this for initialization
	protected virtual void Start () 
    {
        //override me
	}
	
	// Update is called once per frame
	protected virtual void Update () 
    {
        //override me
	}
}
