﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuggestionManager : MonoBehaviour 
{
    public static SuggestionManager instance;
    public float sphereRadius = 300.0f;
    public GameObject linePrefab;
    public SuggestionObject suggestionObjectPrefab;
    private Suggestion[] _suggestions;
    private List<SuggestionObject> _suggestionObjects;
    private Dictionary<int, SuggestionObject> _suggestionObjectsDict;
    private Dictionary<SuggestionCategory, List<SuggestionObject>> _categories;
    private SuggestionObject selectedObject;
    private int _totalVotes;
    private SuggestionSettings _settings;

    public delegate void SuggestionChangeDelegate(SuggestionObject s);

    public SuggestionChangeDelegate OnSuggestionObjectChange;

    public SuggestionSettings settings
    {
        get
        {
            return _settings;
        }
        set
        {
            _settings = value;
        }
    }

    void Awake()
    {
        instance = this;
        _suggestionObjects = new List<SuggestionObject>();
        _suggestionObjectsDict = new Dictionary<int, SuggestionObject>();
        _categories = new Dictionary<SuggestionCategory, List<SuggestionObject>>(); //ordered by last modified.
    }

    public void InitWithSettings(SuggestionSettings s)
    {
        _settings = s;
        /*
        Debug.Log(s.client_control_sensitivity_h);
        Debug.Log(s.client_control_sensitivity_v);
        Debug.Log(s.client_control_sensitivity_z);
        Debug.Log(s.client_data_update_frequency);
        Debug.Log(s.client_zoomed_in_colour);
        Debug.Log(s.client_zoomed_out_colour);
        Debug.Log(s.client_globe_max_size);
        Debug.Log(s.client_globe_min_size);
        */
        DropletLog.Log("Settings", "Using update frequency " + _settings.client_data_update_frequency);
        DropletWebInterface.instance.Invoke("OnGetCategories", 1.0f);
        DropletWebInterface.instance.InvokeRepeating("OnGetChanged", 1.5f, _settings.client_data_update_frequency);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public SuggestionObject GetSuggestionObject(int id)
    {
        if (_suggestionObjectsDict.ContainsKey(id))
            return _suggestionObjectsDict[id];
        else
            return null;
    }

    //Gets called when an individual suggestion collides with the camera.
    public void SetSelected(SuggestionObject s)
    {
        SuggestionObject old = selectedObject;
        selectedObject = s;
        if ( (selectedObject != old) && (selectedObject != null) )
        {
            if (OnSuggestionObjectChange != null)
            {
                OnSuggestionObjectChange(selectedObject);
            }
        }
    }

    public void UnSetSelected(SuggestionObject s)
    {
        if (selectedObject == s)
        {
            selectedObject = null;
            OnSuggestionObjectChange(selectedObject);
        }
            
    }

    public SuggestionObject GetSelected()
    {
        return selectedObject;
    }

    public void AddToTotalVotes(int num)
    {
        _totalVotes += num;
    }

    private SuggestionCategory GetCategory(string categoryname)
    {
        SuggestionCategory retval = null;
        foreach (SuggestionCategory sc in _categories.Keys )
        {
            if (sc.name == categoryname)
            {
                retval = sc;
                break;
            }
        }
        return retval;
    }

    public void AddCategory(SuggestionCategory category)
    {
        if (!_categories.ContainsKey(category) )
            _categories[category] = new List<SuggestionObject>();
    }

    public SuggestionObject AddSuggestion(Suggestion s)
    {
        //Create the object in the 3d world
        SuggestionObject so = Instantiate(suggestionObjectPrefab, Random.onUnitSphere * sphereRadius, Quaternion.identity);
        so.transform.SetParent(transform, false);
        //Get it's category
        SuggestionCategory cat = GetCategory(s.category);
        if (cat == null)
        {
            DropletLog._instance.AddLog("AddSuggestion", "Could not find category " + s.category);
        }
        else
        {
            //Add this suggestion onto the end
            _categories[cat].Add(so);
            //Set the details
            so.SetDetails(s, cat);
            //update total votes
            AddToTotalVotes(s.votes);
        }
        return so;
    }

    public void UpdateSuggestion(int id, Suggestion s)
    {
        SuggestionObject so = _suggestionObjectsDict[id];
        if (so != null)
        {
            int prevotes = so.suggestion.votes;
            int postvotes = s.votes;
            so.SetDetails(s, null );
            AddToTotalVotes(postvotes-prevotes);
        }
    }

    //go through all suggestions and scale them in
    //relation to the total number of votes.
    public void UpdateVoteScales()
    {
        foreach (SuggestionObject so in _suggestionObjectsDict.Values)
        {
            so.ScaleForVotes(_totalVotes);
        }
    }

    public void LinkSuggestionObjects(SuggestionObject s1, SuggestionObject s2)
    {
        GameObject lineObject = Instantiate(linePrefab);
        lineObject.transform.SetParent(s1.GetComponent<Distance>().closeObject.transform, false);
        LineRenderer lr = lineObject.GetComponent<LineRenderer>();
        //Destroy old outline
        if (s1.outLine != null)
            Destroy(s1.outLine.gameObject);

        s1.outLine = lr;
        //if (s2.inLine != null)
        //    Destroy(s1.inLine.gameObject);
        s2.inLine = lr;
        SphericalPath sp = new SphericalPath();
        lr.positionCount = 100;
        lr.SetPositions( sp.MakePath(s1.transform.position, s2.transform.position, transform.position).ToArray() );
    }

    public void SetSuggestions(Suggestion[] suggestions)
    {
        //_suggestionObjects.Clear();
        //_suggestionObjectsDict.Clear();

        foreach (Suggestion s in suggestions)
        {
            if (_suggestionObjectsDict.ContainsKey(s.id) )
            {
                //update
                UpdateSuggestion(s.id, s);
            }
            else
            {
                //add
                _suggestionObjectsDict[s.id] = AddSuggestion(s);
            }
        }

        foreach (SuggestionObject so in _suggestionObjectsDict.Values)
        {
            SuggestionObject nextByCategory = NextByCategory(so);
            if (so != nextByCategory) //if you are not yourself.
            {
                LinkSuggestionObjects(so, nextByCategory);
            }
        }

        //now link things up.
        //if there's more than one object left to process,
        //  pick O1, and O2 which is not O1 and doesn't have an inLine;
        //  create a line prefab which is the outlink for O1 and inlink for O2
        /* 
        foreach (string category in _categories.Keys)
        {
            Debug.Log(category);
            bool exit = false;
            List<SuggestionObject> objs = _categories[category];
            int totalruns = 100;
            while (!exit && (objs.Count > 1) && totalruns > 0 )
            {
                SuggestionObject outSO = FindSOWithNoOutLine(objs, null);
                SuggestionObject inSO = FindSOWithNoInLine(objs, outSO);
                if (outSO == null || inSO == null)
                {
                    exit = true;
                }
                else if (outSO == inSO)
                {
                    Debug.Log("outSO was inSO, try again");
                }
                else
                {
                    GameObject lineObject = Instantiate(linePrefab);
                    lineObject.transform.SetParent(outSO.GetComponent<Distance>().closeObject.transform, false);
                    LineRenderer lr = lineObject.GetComponent<LineRenderer>();
                    outSO.outLine = lr;
                    inSO.inLine = lr;
                    SphericalPath sp = new SphericalPath();
                    lr.positionCount = 100;
                    lr.SetPositions( sp.MakePath(outSO.transform.position, inSO.transform.position, transform.position).ToArray() );
                }
                totalruns -=1;
            }

        }
        */
        UpdateVoteScales();
    }

    public SuggestionObject FindSOWithNoInLine(List<SuggestionObject> sos, SuggestionObject badso)
    {
        SuggestionObject s = null;
        foreach (SuggestionObject so in sos)
        {
            if (so.inLine == null && so != badso)
            {
                s = so;
                break;
            }
        }
        return s;
    }
    public SuggestionObject FindSOWithNoOutLine(List<SuggestionObject> sos, SuggestionObject badso)
    {
        SuggestionObject s = null;
        foreach (SuggestionObject so in sos)
        {
            if (so.outLine == null && so != badso)
            {
                s = so;
                break;
            }
        }
        return s;
    }

    public SuggestionObject NextByVotes(SuggestionObject s)
    {
        return null;
    }

    public SuggestionObject NextByCategory(SuggestionObject s)
    {
        SuggestionObject retval  = null;
        List<SuggestionObject> sos = _categories[s.category];

        int index = sos.IndexOf(s);
        if (index < 0)  //-1 is not found
        {
            retval = s; //return input as output
            DropletLog._instance.AddLog("NextByCategory", "Search item not found in " + s.category.name);
        }
        else
        {
            if (index == (sos.Count-1) ) //if the index is the last element, wrap it back to the first
            {
                index = -1; //will have 1 added to it.
            }
            retval = sos[index+1]; //return the next item in the list
        }
        return retval;
    }

}
