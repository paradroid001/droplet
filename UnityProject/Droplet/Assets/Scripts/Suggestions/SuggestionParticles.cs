﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuggestionParticles : MonoBehaviour 
{

    public SuggestionObject suggestionObject;
    private ParticleSystem _ps;

    [Range(0.0f, 1.0f)]
    public float maxAlpha = 1.0f;

	// Use this for initialization
	void Start () 
    {
	    _ps = GetComponent<ParticleSystem>();
        var col = _ps.colorOverLifetime;
        col.enabled = true;
        Gradient grad = new Gradient();
        grad.SetKeys( new GradientColorKey[] { new GradientColorKey(suggestionObject.categoryColour, 1.0f), 
                                               new GradientColorKey(Color.red, 1.0f) }, 
        new GradientAlphaKey[] { new GradientAlphaKey(maxAlpha, 0.0f), 
                                new GradientAlphaKey(0.0f, maxAlpha) } );

        col.color = grad;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
