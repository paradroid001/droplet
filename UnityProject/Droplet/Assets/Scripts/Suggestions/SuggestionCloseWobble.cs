﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SuggestionCloseWobble : MonoBehaviour 
{
	public float ywobble = 0.1f;
	public float xwobble = 0.1f;
	private float periodx;
	private float periody;
	// Use this for initialization
	void Start () {
		periodx = Random.Range(0.5f, 1.5f);
		periody = Random.Range(0.5f, 1.5f);
		
	}
	
	// Update is called once per frame
	void Update () {
		float xrot = Mathf.Sin(Time.time * periodx) * ywobble;
		float yrot = Mathf.Sin(Time.time * periody) * xwobble;
		transform.Rotate(xrot, yrot, 0.0f);
	}
}
