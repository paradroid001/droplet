﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SphericalPath
{
    public List<Vector3> points;
    public int numPoints = 100;
    
    public SphericalPath()
    {
        points = new List<Vector3>();
    }

    public List<Vector3> MakePath(Vector3 p1, Vector3 p2, Vector3 c)
    {
        Vector3 first = p1-c;
        Vector3 second = p2-c;
        Quaternion q = Quaternion.FromToRotation(first, second);
        float step = 1.0f / numPoints;
        for (int i = 0; i< numPoints; i++)
        {
            Quaternion qstep = Quaternion.Lerp(Quaternion.identity, q, step * i);
            Vector3 newpoint = qstep * first;
            points.Add(newpoint);
        }
        return points;
    }
}

public class SuggestionObject : MonoBehaviour 
{

    public CanvasGroup infoGroup;
    public LineRenderer inLine;
    public LineRenderer outLine;
    public GameObject linePrefab;
    public int id;
    public SuggestionCategory category;
    public Text categoryText;
    public Text suggestionText;
    public Text dateText;
    public GameObject menuObject;
    public GameObject closeObject;
    public GameObject farObject;

    public Color categoryColour;

    private Color textColour;
    private Color fadedTextColour;
    private SuggestionManager manager;
    private Suggestion _suggestion;


    public Suggestion suggestion
    {
        get {return _suggestion;}
    }

    public int votes
    {
        get {return _suggestion.votes;}
    }

	// Use this for initialization
	void Awake () {
		manager = FindObjectOfType<SuggestionManager>();
        menuObject.SetActive(false);

        //Some default colours.
        categoryColour = Color.red;
        //textColour = categoryColour + new Color(0.6f, 0.6f, 0.6f, 1.0f);
        //fadedTextColour = textColour;
        //fadedTextColour.a = 0.25f;
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        
	    if (outLine!=null && outLine.gameObject.activeSelf)
        {
            //outLine.SetPosition(0, transform.position);

            SphericalPath sp = new SphericalPath();
            //lr.positionCount = 100;
            outLine.SetPositions( sp.MakePath(transform.position, outLine.GetPosition(outLine.positionCount-1), manager.transform.position).ToArray() );

        }

        if (inLine !=null)
        {
            inLine.SetPosition(inLine.positionCount-1, transform.position);
        }
	}

    public Quaternion GetOutDestinationRotation()
    {
        Quaternion retval = Quaternion.identity;
        if (outLine != null)
        {
            Vector3 first = outLine.GetPosition(0)-manager.transform.position;
            Vector3 second = outLine.GetPosition(outLine.positionCount-1)-manager.transform.position;
            retval = Quaternion.FromToRotation(first, second);

        }
        return retval;

    }

    //Pass null for cat if you're just updating details.
    public void SetDetails(Suggestion s, SuggestionCategory cat)
    {
        id = s.id;
        _suggestion = s;
        categoryText.text = s.question;
        suggestionText.text = "\"" + s.text + "\"";
        if (s.suggestor != null && s.suggestor != "")
        {
            dateText.text = "Submitted by: " + s.suggestor;
        }
        else
        {
            dateText.text = "";
        }
        /*
        Debug.Log("Suggestion Text: " + s.text);
        Debug.Log("Suggestion Style: " + s.style);
        Debug.Log("Suggestor: " + s.suggestor);
        Debug.Log("Votes: " + s.votes);
        Debug.Log("Question: " + s.question);
        */
        
        if (cat != null)
        {
            category = cat;
            string col = cat.colours[ Random.RandomRange(0, cat.colours.Length-1)];
            //Debug.Log("Colour: " + col);
            if (!ColorUtility.TryParseHtmlString(col, out categoryColour))
            {
                DropletLog._instance.AddLog("SetDetails", "Could not parse colour " + col);
                categoryColour = Color.yellow;
            }
        
            //textColour = Color.white;
            
            //textColour = categoryColour + new Color(0.6f, 0.6f, 0.6f, 1.0f);
            //fadedTextColour = textColour;
            //fadedTextColour.a = 0.25f;
        }
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "MainCamera")
        {
            OnFocus();
        }
    }

    public void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "MainCamera")
        {
            OnUnFocus();
        }
    }

    public void OnReveal()
    {
        //StartCoroutine(FadeText( suggestionText, 1.0f, fadedTextColour));
        
        
        //categoryText.color = new Color(1, 1, 1, 0);
        //dateText.color = new Color(1, 1, 1, 0);
        
        
        //StartCoroutine(FadeIn( suggestionText, 1.0f, Color.white));
        //StartCoroutine(FadeIn( suggestionText, 1.0f, Color.white));
    }

    public void OnHide()
    {
    }

    public void OnClick()
    {
        menuObject.SetActive(true);
    }

    public void OnFocus()
    {
        //Debug.Log("OnFocus");
        //StartCoroutine(FadeText( suggestionText, 1.0f, textColour));
        //StartCoroutine(FadeText( categoryText, 1.0f, textColour));
        //StartCoroutine(FadeText( dateText, 1.0f, textColour));
        
        
        //suggestionText.color = textColour;
        //categoryText.color = textColour;
        //dateText.color = textColour;

        manager.SetSelected(this);
    }

    public void OnUnFocus()
    {
        //Debug.Log("OnUnFocus");
        //StartCoroutine(FadeText( suggestionText, 1.0f, fadedTextColour));
        //StartCoroutine(FadeText( categoryText, 1.0f, new Color(1, 1, 1, 0f)));
        //StartCoroutine(FadeText( categoryText, 1.0f, new Color(1, 1, 1, 0f)));
        
        //suggestionText.color = fadedTextColour;
        //categoryText.color = fadedTextColour;
        //dateText.color = fadedTextColour;
        
        
        manager.UnSetSelected(this);
    }

    public void ScaleForVotes(int totalVotes)
    {
        return;
        float newscale = Mathf.Max(1.0f, 1.0f*votes);
        farObject.transform.localScale = new Vector3(newscale, newscale, newscale);
    }

    private IEnumerator FadeText(Text text, float time, Color destColor)
    {
        yield return null; //exit straight away
        float timer = 0.0f;
        Color startColor = destColor;
        startColor.a = 0.0f;
        while (timer < time)
        {
            timer += Time.deltaTime;
            text.color = Color.Lerp(startColor, destColor, timer/time);
            yield return new WaitForSeconds(0.05f);
        }
        yield return null;
    }

}
