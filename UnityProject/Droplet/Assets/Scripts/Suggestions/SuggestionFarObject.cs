﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class SuggestionFarObject : MonoBehaviour 
{
    
    public SuggestionObject suggestionObject;
    public SpriteRenderer sprite;
    private float turnRate;

	// Use this for initialization
	void Start () 
    {
        turnRate =Random.Range(10.0f, 80.0f);
	    sprite.color = suggestionObject.categoryColour;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    sprite.transform.Rotate(0, 0, turnRate * Time.deltaTime);	
	}
}
