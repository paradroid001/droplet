﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using Utils;
using UnityEngine.UI;

[System.Serializable]
public class DropletRemoteInput
{
    public float vertical;
    public float horizontal;
    public float zoom;
    public bool vote = false;
    public bool next = false;
    public bool newest = false;
    public bool random = false;
    public bool restart = false;
    public bool popular = false;
    public byte[] Serialize()
    {
        return ObjectSerializationExtension.SerializeToByteArray(this);
    }

}

public class DropletNetworkClient : MonoBehaviour, INetEventListener
 {
    public Image connectedImage;
    public Image disconnectedImage;
    private NetClient _netClient;
    private NetPeer _ourPeer;
    private NetDataWriter _dataWriter;

    private bool _newData;
    private DropletRemoteInput _inputPackage;

	// Use this for initialization
	void Start () 
	{
		_netClient = new NetClient(this, "Droplet");
	    _netClient.Start();
	    _netClient.UpdateTime = 60;
        _dataWriter = new NetDataWriter();
        _newData = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		_netClient.PollEvents();

        connectedImage.gameObject.SetActive(_netClient.IsConnected && _ourPeer != null);
        disconnectedImage.gameObject.SetActive(!( _netClient.IsConnected && _ourPeer != null) );

        if (_netClient.IsConnected)
        {
            if (_ourPeer != null && _newData == true)
            {
                _dataWriter.Reset();
                //Send it
                byte[] bytes = _inputPackage.Serialize();
                _dataWriter.Put(bytes, 0, bytes.Length);
                //_dataWriter.Put(_inputPackage.vertical);
                _ourPeer.Send(_dataWriter, SendOptions.Sequenced);
                _newData = false;
            }
        }
        else
        {
            _netClient.SendDiscoveryRequest(new byte[] { 1 }, 5000);
        }	
	}

	public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
        byte[] bytes = reader.GetBytes();
        DropletRemoteFeedback f = ObjectSerializationExtension.Deserialize<DropletRemoteFeedback>(bytes);
        if (f != null)
        {
            DropletRemote.instance.VoteButtonEnable(f);
        }
    }

    void OnDestroy()
    {
        if(_netClient != null)
            _netClient.Stop();
    }

	public void OnPeerConnected(NetPeer peer)
    {
        DropletLog._instance.AddLog("OnPeerConnected", "We connected to " + peer.EndPoint);
        _ourPeer = peer;
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectReason reason, int socketErrorCode)
    {
        DropletLog._instance.AddLog("OnPeerDisconnected", "We disconnected because " + reason);
        if (peer == _ourPeer)
            _ourPeer = null;
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        DropletLog._instance.AddLog("OnNetworkError", "We received error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryResponse && _netClient.Peer == null)
        {
            DropletLog._instance.AddLog("OnNetworkRecieveUnconnected", "Received discovery response. Connecting to: " + remoteEndPoint);
            _netClient.Connect(remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }

    public void NewInput(DropletRemoteInput r)
    {
        _newData = true;
        _inputPackage = r;
    }
}
