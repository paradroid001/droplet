﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropletRemote : MonoBehaviour {

	public Button voteButton;
	public Text buttonText;
	static DropletRemote _instance;
	// Use this for initialization

	public static DropletRemote instance
	{
		get { return _instance; }
	}

	void Awake()
	{
		_instance = this;
	}
	void Start () {
		VoteButtonEnable(null);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void VoteButtonEnable(DropletRemoteFeedback val)
	{
		if (voteButton != null)
		{
			if (val == null)
			{
				voteButton.interactable = false;
			}
			else 
			{
				Color c;
				Debug.Log(val.selectedObjectColour);
				if (val.objectSelected 
					&& ColorUtility.TryParseHtmlString("#" + val.selectedObjectColour, out c)) 
				{
					Debug.Log(c);
					ColorBlock cb = voteButton.colors;
        			cb.normalColor = c;
        			voteButton.colors = cb;
				}
				voteButton.interactable = val.objectSelected;
				if (val.objectSelected)
				{
					buttonText.color = Color.white;
				}
				else
				{
					buttonText.color = Color.black;
				}	
			}
		}
	}
}
