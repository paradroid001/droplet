using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
public class DropletJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
	public enum AxisOption
	{
		// Options for which axes to use
		Both, // Use both
		OnlyHorizontal, // Only horizontal
		OnlyVertical // Only vertical
	}

	public int MovementRange = 100;
	public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
	public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
	public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input

	Vector3 m_StartPos;
	bool m_UseX; // Toggle for using the x axis
	bool m_UseY; // Toggle for using the Y axis
	CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
	CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input

	public Canvas parentCanvas;

	void OnEnable()
	{
		//Debug.Log("Joystick Enable");
		CreateVirtualAxes();	
	}

	void Start()
	{
		//For some reason getting the transition in Start, for this script,
		//always returns 0,0,0. So instead we get it below in the first 
		//OnPointerDown event.
		m_StartPos = Vector3.zero; //transform.localPosition;
		//Debug.Log("joy start pos = " + m_StartPos);
		//Debug.Log("joy transform pos = " + transform.position);
	}

	void UpdateVirtualAxes(Vector3 value)
	{
		var delta = m_StartPos - value;
		delta.y = -delta.y;
		delta /= MovementRange;
		//Debug.Log("DELTA" + delta);
		if (m_UseX)
		{
			m_HorizontalVirtualAxis.Update(-delta.x);
			//m_HorizontalVirtualAxis.SetAxis(-delta.x);
			//Debug.Log("updated x" + m_HorizontalVirtualAxis.GetValue);
			//CrossPlatformInputManager.SetAxis("Horizontal", -delta.x);
			
		}

		if (m_UseY)
		{
			m_VerticalVirtualAxis.Update(delta.y);
			//m_VerticalVirtualAxis.SetAxis(delta.y);
		}
		//Debug.Log("updated x: " + CrossPlatformInputManager.GetAxis("Horizontal") + 
		//                 " y: " + CrossPlatformInputManager.GetAxis("Vertical"));
	}

	void CreateVirtualAxes()
	{
		// set axes to use
		m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
		m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

		// create new axes based on axes to use
		if (m_UseX)
		{
			m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
		}
		
	}


	public void OnDrag(PointerEventData data)
	{
		
		Vector3 newPos = Vector3.zero;
		//Debug.Log("DATA: " + data);
		if (m_UseX)
		{
			int delta = (int)(data.position.x - m_StartPos.x);
			delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
			newPos.x = delta;
		}

		if (m_UseY)
		{
			int delta = (int)(data.position.y - m_StartPos.y);
			delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
			newPos.y = delta;
		}
		transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
		
		//transform.position += (Vector3)data.delta;
		UpdateVirtualAxes(transform.position);
	}


	public void OnPointerUp(PointerEventData data)
	{
		transform.position = m_StartPos;
		UpdateVirtualAxes(m_StartPos);
	}


	public void OnPointerDown(PointerEventData data) 
	{ 
		//Debug.Log(transform.position);
		//Debug.Log(m_StartPos);
		
		
		if (m_StartPos == Vector3.zero)
		{
			m_StartPos = transform.position;
		}
		
	}

	void OnDisable()
	{
		//Debug.Log("Joystick Disable");
		// remove the joysticks from the cross platform input
		if (m_UseX)
		{
			m_HorizontalVirtualAxis.Remove();
		}
		if (m_UseY)
		{
			m_VerticalVirtualAxis.Remove();
		}
	}
}