﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class DropletRemoteControl : MonoBehaviour 
{

	public GameObject debugUI;
	public Text inputDebugText;
	private DropletNetworkClient _DNC;
	//private DropletRemoteInput _input;
	bool votePressed = false;
	bool nextPressed = false;
	bool newestPressed = false;
	bool popularPressed = false;
	bool randomPressed = false;
	//bool quitPressed = false;
	bool restartPressed = false;
	public bool startUIOff = true;

	void Awake()
	{
		_DNC = GetComponent<DropletNetworkClient>();
	}

	// Use this for initialization
	void Start () 
	{
		if (startUIOff && debugUI)
		{
			debugUI.SetActive(false);
		}
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		float v = CrossPlatformInputManager.GetAxis("Vertical");
		float h = CrossPlatformInputManager.GetAxis("Horizontal");
		
		float z = 0.0f;
		
		if ( CrossPlatformInputManager.GetButton("ZoomIn") )
		{
			z = 1.0f;
			//Debug.Log("Zoom In");
		}
		if ( CrossPlatformInputManager.GetButton("ZoomOut") )
		{
			z = -1.0f;
			//Debug.Log("Zoom OUt");
		}
		//float rv = Input.GetAxisRaw("Vertical");
		inputDebugText.text = ("V: " + v + ", H: " + h);

		if (v != 0.0f || h != 0.0f 
		|| newestPressed 
		|| nextPressed 
		|| votePressed
		|| randomPressed
		|| popularPressed
		|| restartPressed 
		|| z != 0.0f)
		{
			DropletRemoteInput i = new DropletRemoteInput();
			i.vertical = v;
			i.horizontal = h;
			i.zoom = z;
			i.vote = votePressed;
			i.newest = newestPressed;
			i.next = nextPressed;
			i.random = randomPressed;
			i.popular = popularPressed;
			i.restart = restartPressed;
			_DNC.NewInput(i);

			//Reset the values
			votePressed = newestPressed = nextPressed = popularPressed = randomPressed = restartPressed = false;
		}
	}

	public void OnVotePressed(Button b)
	{
		votePressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Vote Pressed");
		b.interactable = false; //The Droplet Remote will re-enable when refocussed.
	}

	public void OnNextPressed()
	{
		nextPressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Next Pressed");
	}

	public void OnNewestPressed()
	{
		newestPressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Newest Pressed");
	}

	public void OnPopularPressed()
	{
		popularPressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Popular Pressed");
	}

	public void OnRandomPressed()
	{
		randomPressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Random Pressed");
	}

	public void OnRestartPressed()
	{
		restartPressed = true;
		DropletLog._instance.AddLog("RemoteControl", "Restart Pressed");
	}
}
