﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
//using UnityEngine.PostProcessing.Utilities;

public class DropletControl : MonoBehaviour 
{
    public bool startUIOff;
    public DropletWebInterface dwi;
    public Camera sceneCamera;
    public Rigidbody sceneCameraRB;
    public GameObject UI;
    public float zoomSpeed = 400.0f;
    public float rotateSpeed = 1.0f;
    public AudioSource movementSound;
    private Vector2 _prevMousePos = new Vector2(0, 0);

    public bool autoFocusOnSelected = true;
    private bool lockoutInput = false;

    private SuggestionManager manager;
    //private FocusPuller _focusPuller;
    private Rigidbody _rb;

    private Vector3 _cameraZoomOutPos;
   

	// Use this for initialization
	void Start () 
    {
		Cursor.lockState = CursorLockMode.Locked;
        manager = GetComponent<SuggestionManager>();
        if (autoFocusOnSelected)
            manager.OnSuggestionObjectChange += FocusOnSelected;
        sceneCameraRB = sceneCamera.GetComponent<Rigidbody>();
        dwi.Invoke("OnGetSettings", 1.0f);
        _rb = GetComponent<Rigidbody>();
        //_focusPuller = sceneCamera.GetComponent<FocusPuller>();
        if (startUIOff && UI.activeSelf) //start UI off
            ToggleUI();

        //cache the zoom out pos
        _cameraZoomOutPos = sceneCamera.transform.position;
	}
	
    public void FocusOnSelected(SuggestionObject s)
    {
        //Debug.Log("Focus time");
        //_focusPuller.target = s.gameObject.transform;
    }

    public void RemoteInput(DropletRemoteInput ri)
    {
        if (!lockoutInput)
        {
            //invert horizontal
            float v = ri.vertical * rotateSpeed * SuggestionManager.instance.settings.client_control_sensitivity_v;
            float h = ri.horizontal * rotateSpeed * SuggestionManager.instance.settings.client_control_sensitivity_h;
            float z = ri.zoom * zoomSpeed * SuggestionManager.instance.settings.client_control_sensitivity_z;
            transform.Rotate(v, h, 0.0f, Space.World);
            //slow the zoom
            sceneCameraRB.MovePosition(sceneCamera.transform.position + sceneCamera.transform.forward * z  * Time.deltaTime);
            
            SuggestionObject selectedObject = manager.GetSelected();
            if (selectedObject != null)
            {
                if (ri.vote)
                {
                    //Debug.Log("VotePressed");
                    DropletLog._instance.AddLog("RemoteInput", "Vote Pressed");
                    dwi.OnVote(selectedObject.id);
                }
                if (ri.next)
                {
                    Debug.Log("NextPressed");
                    SuggestionObject from = selectedObject;
                    SuggestionObject to = manager.NextByCategory(from);
                    StartCoroutine( RotateOverTime(from.transform.position, 
                                                to.transform.position, 
                                                2.0f) );
                }
               
            }

            if (ri.newest)
            {
                DropletLog._instance.AddLog("RemoteInput", "Newest Pressed");
                dwi.OnNewest();
            }
            if (ri.random)
            {
                DropletLog._instance.AddLog("RemoteInput", "Random Pressed");
                dwi.OnRandom();
            }
            if (ri.popular)
            {
                dwi.OnPopular();
            }
            if (ri.restart)
            {
                DropletLog._instance.AddLog("RemoteInput", "Restart Pressed");
            }
        }
    }

    public void ToggleUI()
    {
        UI.SetActive(!UI.activeSelf);
    }

	// Update is called once per frame
	void Update () 
    {


	    //float v = CrossPlatformInputManager.GetAxis("Vertical");
	    //float h = CrossPlatformInputManager.GetAxis("Horizontal");
        
        //Vector2 mouseDelta = (Vector2)Input.mousePosition - _prevMousePos;
        //transform.Rotate(mouseDelta.x * rotateSpeed, mouseDelta.y * rotateSpeed, 0.0f);
        //_prevMousePos = Input.mousePosition;
        
        if (!lockoutInput)
        {
            Vector3 motion = new Vector3(Input.GetAxis("Mouse Y") * rotateSpeed, -Input.GetAxis("Mouse X") * rotateSpeed, 0.0f);
            //transform.Rotate(Input.GetAxis("Mouse Y") * rotateSpeed, -Input.GetAxis("Mouse X") * rotateSpeed, 0.0f, Space.World);
            
            if ( SuggestionManager.instance.GetSelected() != null)
            {
                _rb.angularVelocity = Vector3.zero;
                transform.Rotate(motion.x, motion.y, motion.z, Space.World);
            }
            else
            {
                if (motion.magnitude > 0.0f)
                {
                    _rb.angularVelocity = new Vector3(motion.x, motion.y, motion.z);
                }
            
            }

            Vector2 zoom = Input.mouseScrollDelta;
            sceneCameraRB.MovePosition(sceneCamera.transform.position + sceneCamera.transform.forward * zoom.y * zoomSpeed * Time.deltaTime);

            if (Input.GetMouseButtonDown(0))
            {
                Debug.Log("Tap");
                if (manager.GetSelected() != null)
                {
                    manager.GetSelected().OnClick();
                    //StartCoroutine(manager.RotateBy(manager.GetSelected().GetOutDestinationRotation(), 2.0f) );
                }
            }

            SuggestionObject selectedObject = manager.GetSelected();
            if (selectedObject != null)
            {
                
                //Rotate to next item
                if (Input.GetKeyDown(KeyCode.R) )
                {
                    SuggestionObject from = selectedObject;
                    SuggestionObject to = manager.NextByCategory(from);
                    StartCoroutine( RotateOverTime(from.transform.position, 
                                                to.transform.position, 
                                                2.0f) );
                }
            
                if (Input.GetKeyDown(KeyCode.V))
                {
                    dwi.OnVote(selectedObject.id);
                }
            }

        }

        if (movementSound != null)
        {
            movementSound.volume = _rb.angularVelocity.magnitude * rotateSpeed;
        }
        /*
        //do a camera raycast
        RaycastHit hit;
        if (Physics.Raycast(sceneCamera.transform.position, sceneCamera.transform.forward, out hit, 150.0f) )
        {
            if (hit.collider != null)
            {
                SuggestionObject suggestion = hit.collider.gameObject.GetComponent<SuggestionObject>();
                if (suggestion != null)
                {
                    suggestion.OnView();
                }
            }
        }
        */
        
        if (Input.GetKeyDown (KeyCode.End))
		{
            Cursor.lockState = CursorLockMode.None;
			//Screen.lockCursor = (Screen.lockCursor == false) ? true : false;
		}

        //Pull suggestions
        if (Input.GetKeyDown(KeyCode.Space))
        {
            dwi.OnGetChanged();
        }

        //Pull Categories
        if (Input.GetKeyDown(KeyCode.C))
        {
            dwi.OnGetCategories();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleUI();
        }

    
	}

    public void GoToSuggestion(SuggestionObject s)
    {
        StartCoroutine(RotateOverTime(sceneCamera.transform.position, 
                                      s.transform.position, 2.0f) );
        StartCoroutine(ZoomCameraOutIn(2.0f));
    }
    
    public IEnumerator RotateOverTime(Vector3 fromDir, Vector3 toDir, float t)
    {
        lockoutInput = true;
        Quaternion q = Quaternion.FromToRotation(fromDir,
                                                toDir);
        q = Quaternion.Inverse(q);
        
        GameObject go = new GameObject();
        go.transform.rotation = transform.rotation;
        go.transform.Rotate(q.eulerAngles, Space.World);
        Quaternion fromrot = transform.rotation;
        Quaternion endrot = go.transform.rotation;

        //Vector3 euler = q.eulerAngles * 1.0f;
        Vector3 euler = q.eulerAngles * Mathf.Deg2Rad; //q.ToEulerAngles();
        float timer = 0.0f;
        while (timer <= t)
        {
            timer += Time.deltaTime;
            transform.rotation = Quaternion.Lerp(fromrot, endrot, timer/t);
            yield return null;
        }
        lockoutInput = false;
    }

    public IEnumerator ZoomCameraOutIn(float t)
    {
        Vector3 diff = sceneCamera.transform.position - _cameraZoomOutPos;
        float timer = 0.0f;
        while (timer < t)
        {
            timer += Time.deltaTime;
            float radians = Mathf.Deg2Rad * (180.0f * timer/t );
            sceneCameraRB.MovePosition( _cameraZoomOutPos + ( diff * (1.0f- Mathf.Sin(radians)) ) );
            yield return null;
        }
    }
}
