﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;
using Utils;

[System.Serializable]
public class DropletRemoteFeedback
{
    public bool objectSelected;
    public int selectedObjectID;
    public string selectedObjectColour;
    public byte[] Serialize()
    {
        return ObjectSerializationExtension.SerializeToByteArray(this);
    }

    public void Unselect()
    {
        objectSelected = false;
        selectedObjectID = -1;
    }
}

public class DropletNetworkListener : MonoBehaviour, INetEventListener
{
	private NetServer _netServer;
    private NetPeer _ourPeer;
    private NetDataWriter _dataWriter;

    private DropletControl _control;

    private DropletRemoteFeedback _feedback;
    private bool _newData;

	public void Start()
	{
        _control = GetComponent<DropletControl>();
		_netServer = new NetServer(this, 100, "Droplet");
		_netServer.Start(5000);
		_netServer.DiscoveryEnabled = true;
		_netServer.UpdateTime = 15;
		DropletLog._instance.AddLog("Listener Start", "Server listening");
        _dataWriter = new NetDataWriter();
        _newData = false;
        _feedback = new DropletRemoteFeedback();
        _feedback.Unselect();
        SuggestionManager.instance.OnSuggestionObjectChange += Feedback;
    }

	// Update is called once per frame
	void Update () 
    {
	    _netServer.PollEvents();
        //if (_netServer.IsConnected)
        //{
            if (_ourPeer != null && _newData == true)
            {
                _dataWriter.Reset();
                //Send it
                byte[] bytes = _feedback.Serialize();
                _dataWriter.Put(bytes, 0, bytes.Length);
                _ourPeer.Send(_dataWriter, SendOptions.Sequenced);
                _newData = false;
            }
        //}
	}

	void OnDestroy()
    {
        if(_netServer != null)
            _netServer.Stop();
    }
    void FixedUpdate()
    {
        //if (_ourPeer != null)
        //{
           //queue data to send?
        //}
    }

    //send feedback to the remote
    public void Feedback(SuggestionObject s)
    {
        //Debug.Log("Feedback got new object");
        if (s != null && _feedback.selectedObjectID != s.id)
        {
            //then this is new data
            _feedback.selectedObjectID = s.id;
            _feedback.objectSelected = true;
            _feedback.selectedObjectColour = ColorUtility.ToHtmlStringRGB(s.categoryColour);
            _newData = true;
        }
        else if (s == null && _feedback.objectSelected == true)
        {
            //things have become 'unselected'
            _feedback.selectedObjectID = -1;
            _feedback.objectSelected = false;
            _newData = true;
        }
        else
        {
            _newData = false;
        }
    }
	public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
		//DropletLog._instance.AddLog("OnNetworkReceive", "Received data");
        //Debug.Log("Received! " + Time.time);
        byte[] bytes = reader.GetBytes();
        DropletRemoteInput ri = ObjectSerializationExtension.Deserialize<DropletRemoteInput>(bytes);
        _control.RemoteInput(ri);
    }

	public void OnPeerConnected(NetPeer peer)
    {
        DropletLog._instance.AddLog("OnPeerConnected", "[SERVER] We have new peer " + peer.EndPoint);
        _ourPeer = peer;
    }
	
	public void OnPeerDisconnected(NetPeer peer, DisconnectReason reason, int socketErrorCode)
    {
        DropletLog._instance.AddLog("OnPeerDisconnected", "[SERVER] peer disconnected " + peer.EndPoint + ", info: " + reason);
        if (peer == _ourPeer)
            _ourPeer = null;
    }

	public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        DropletLog._instance.AddLog("OnNetworkError", "[SERVER] error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryRequest)
        {
            DropletLog._instance.AddLog("OnNetworkReceiveUnconnected", "[SERVER] Received discovery request. Send discovery response");
            _netServer.SendDiscoveryResponse(new byte[] {1}, remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }

}
