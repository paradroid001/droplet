﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SuggestionSettings
{
    /* See models.py */
    public int client_orb_limit;
    public string client_orb_selection;
    public float client_globe_max_size;
    public float client_globe_min_size;
    public int client_data_update_frequency; //in seconds
    public string client_zoomed_out_colour; //hex colour
    public string client_zoomed_in_colour; //hex colour
    public float client_control_sensitivity_h; //horiz
    public float client_control_sensitivity_v; //vert
    public float client_control_sensitivity_z; //zoom
    
}

[System.Serializable]
public class Suggestion
{
    public int id;
    public string text;
    public string question;
    public string category;
    public string enabled;
    public string style;
    public string suggestor;
    public string last_modified_date;
    public int votes;
}

[System.Serializable]
public class SuggestionList
{
    public Suggestion[] suggestions;
}

[System.Serializable]
public class SuggestionCategory
{
    public string name;
    public bool enabled;
    public string[] colours;
    public string image;
    public string last_modified_date;
    //public string categoryHexColour; 
    //public string categoryTextColour;
}


[System.Serializable]
public class SuggestionCategoryList
{
    public SuggestionCategory[] categories;
}

public class DropletWebInterface : MonoBehaviour 
{
    public static DropletWebInterface instance;
    public string serverURL = "http://127.0.0.1:8000";
    private HTTP _http;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

	// Use this for initialization
	void Start () 
    {
	    _http = GetComponent<HTTP>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnGetSettings()
    {
        string url = serverURL + "/suggestion/settings/";
        _http.get(url, OnGetSettingsCB);    
    }

    public void OnGetCategories()
    {
        string url = serverURL + "/suggestion/categories/";
        _http.get(url, OnGetCategoriesCB );
    }

    public void OnGetChanged()
    {
        string url = serverURL + "/suggestion/all/";
        _http.get(url, OnGetChangedCB );
    }

    public void OnVote(int suggestionid)
    {
        string url = serverURL + "/suggestion/vote/" + suggestionid + "/";
        _http.get(url, OnVotedCB);
    }

    public void OnNewest()
    {
        string url = serverURL + "/suggestion/newest/";
        _http.get(url, OnNewestCB);

    }

    public void OnRandom()
    {
        string url = serverURL + "/suggestion/random/";
        _http.get(url, OnRandomCB);

    }

    public void OnPopular()
    {
        string url = serverURL + "/suggestion/popular/";
        _http.get(url, OnPopularCB);

    }

    private void OnGetChangedCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        //Debug.Log(ar.datastr.Length);
        //Debug.Log(ar.errorstr.ToString());
        //Debug.Log("Datastr: " + ar.datastr);
        SuggestionList s = JsonUtility.FromJson<SuggestionList>(ar.datastr);
        //Debug.Log(s);
        //Debug.Log(s.suggestions);
        //foreach (Suggestion suggestion in s.suggestions)
        //{
        //    Debug.Log(suggestion);
        //}
        SuggestionManager.instance.SetSuggestions(s.suggestions);
    }

    private void OnGetCategoriesCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Get Categories", ar.errorstr.ToString() );
        }
        else
        {
            SuggestionCategoryList scs = JsonUtility.FromJson<SuggestionCategoryList>(ar.datastr);
            foreach (SuggestionCategory sc in scs.categories)
            {
                //Debug.Log(sc);
                SuggestionManager.instance.AddCategory(sc);
            }
        }
    } 

    private void OnVotedCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Vote: ", ar.errorstr.ToString() );
        }
        else
        {
            Suggestion s = JsonUtility.FromJson<Suggestion>(ar.datastr);
            {
                Debug.Log("Vote return: " + s.id);
                SuggestionManager.instance.UpdateSuggestion(s.id, s);
                SuggestionManager.instance.UpdateVoteScales();
            }
        }
    }

    private void OnNewestCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Get Newest: ", ar.errorstr.ToString() );
        }
        else
        {
            Suggestion s = JsonUtility.FromJson<Suggestion>(ar.datastr);
            {
                //Debug.Log("Get Newest return: " + s.id);
                DropletControl c = (DropletControl)FindObjectOfType(typeof(DropletControl));
                SuggestionObject so = SuggestionManager.instance.GetSuggestionObject(s.id);
                if (so!=null && c != null)
                {
                    c.GoToSuggestion(so);
                }
                else
                {
                    DropletLog._instance.AddLog("Get Newest", "Could not find suggestion with id " + s.id);
                }
                //SuggestionManager.instance.UpdateSuggestion(s.id, s);
                //SuggestionManager.instance.UpdateVoteScales();
            }
        }
    }

    private void OnRandomCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Get Random ", ar.errorstr.ToString() );
        }
        else
        {
            Suggestion s = JsonUtility.FromJson<Suggestion>(ar.datastr);
            {
                Debug.Log("Get Random return: " + s.id);
                //SuggestionManager.instance.UpdateSuggestion(s.id, s);
                //SuggestionManager.instance.UpdateVoteScales();
                DropletControl c = (DropletControl)FindObjectOfType(typeof(DropletControl));
                SuggestionObject so = SuggestionManager.instance.GetSuggestionObject(s.id);
                if (so!=null && c != null)
                {
                    c.GoToSuggestion(so);
                }
                else
                {
                    DropletLog._instance.AddLog("Get Random", "Could not find suggestion with id " + s.id);
                }
            }
        }
    }

    private void OnPopularCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Get Popular ", ar.errorstr.ToString() );
        }
        else
        {
            Suggestion s = JsonUtility.FromJson<Suggestion>(ar.datastr);
            {
                Debug.Log("Get Popular return: " + s.id);
                //SuggestionManager.instance.UpdateSuggestion(s.id, s);
                //SuggestionManager.instance.UpdateVoteScales();
                DropletControl c = (DropletControl)FindObjectOfType(typeof(DropletControl));
                SuggestionObject so = SuggestionManager.instance.GetSuggestionObject(s.id);
                if (so!=null && c != null)
                {
                    c.GoToSuggestion(so);
                }
                else
                {
                    DropletLog._instance.AddLog("Get Popular", "Could not find suggestion with id " + s.id);
                }
            }
        }
    }

    private void OnGetSettingsCB(WWW wwwresult)
    {
        ActionResponse ar = ActionResponse.FromWWW(wwwresult);
        if (ar.success == false)
        {
            DropletLog._instance.AddLog("Get Settings", ar.errorstr.ToString() );
        }
        else
        {
            SuggestionSettings ss = JsonUtility.FromJson<SuggestionSettings>(ar.datastr);
            SuggestionManager.instance.InitWithSettings(ss);
        }
    }
}
