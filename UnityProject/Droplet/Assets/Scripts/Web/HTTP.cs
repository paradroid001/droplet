﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

[System.Serializable]
public class ActionResponse
{
    //public code;
    public bool success;
    public string errorstr; //error text string
    public string datastr;  //data string
    public string userstr;  //user facing text (if any)

    public static ActionResponse FromWWW(WWW wwwobject)
    {
        ActionResponse ar;
        if (!string.IsNullOrEmpty(wwwobject.error) )
        {
            ar = new ActionResponse();
            ar.success = false;
            ar.errorstr = wwwobject.error;
        }
        else
        {
            try
            {
                ar = JsonUtility.FromJson<ActionResponse>(wwwobject.text);
            }
            catch
            {
                ar = new ActionResponse();
                ar.success = false;
                ar.errorstr = "Could not parse JSON from reponse";
                ar.datastr = wwwobject.text;
            }
        }
        return ar;
    }

}

public class PostDict
{
    public Dictionary<string, string> data;
    public PostDict()
    {
        data = new Dictionary<string, string>();
    }

    public void At(string key, string val)
    {
        data[key] = val;
    }

    public string At(string key)
    {
        if (data.ContainsKey(key) )
            return data[key];
        else
            return "";
    }
}

public class HTTP : MonoBehaviour 
{

    public delegate void HTTPCallback(WWW wwwobject);
    //Once cookie is set, all gets and posts will be sent
    //with the cookie included - i.e. the sessionid for
    //authenticated sessions. Clear the cookie to "" and
    //this will stop happening.
    public string cookie;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static string get_header(string name, WWW www)
    {
        foreach (KeyValuePair<string, string> kv in www.responseHeaders)
        {
            //Debug.Log(kv.Key);
            if (kv.Key == name)
            {
                return kv.Value;
            }
        }
        return "";
    }

    public Dictionary<string, string> ParseCookie(string cookiestr)
    {
        Dictionary<string, string> retval = new Dictionary<string, string>();
        string regexp = "(?<name>[^=]+)=(?<val>[^;]+)+;?";
        MatchCollection myMatchCollection = Regex.Matches(cookiestr, regexp);
        foreach (Match myMatch in myMatchCollection)
                {
                    string cookieName = myMatch.Groups["name"].ToString().Trim();
                    string cookieVal = myMatch.Groups["val"].ToString().Trim();
                    retval.Add(cookieName, cookieVal);                
                }
        return retval;
    }

    public void get(string url, HTTPCallback cb)
    {
        WWW www = CreateWWW(url, null, cookie);
        /*
        if (cookie != "")
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Cookie", cookie);
            www = new WWW(url, null, headers);
        }
        else
        {
            www = new WWW(url);
        }
        */
        StartCoroutine(WaitForRequest(www, cb));
    }

    public void post(string url, PostDict pd, HTTPCallback cb)
    {
        /*
        WWWForm form = new WWWForm();
        foreach (string key in pd.data.Keys)
        {
            form.AddField(key, pd.data[key]);
        }
        WWW www;
        if (cookie != "")
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Cookie", cookie);
            www = new WWW(url, form, cookie);
        }
        else
        {   
            www = new WWW(url, form);
        }*/
        WWW www = CreateWWW(url, pd, cookie);
        StartCoroutine(WaitForRequest(www, cb));
    }

    //Make a new WWW object.
    //If you have a cookie, it can either go in the headers
    //or in the form fields. But you won't have form fields
    //except if you passed in a post dict.
    //So. Logic is:
    //If you have post data, create form fields.
    //If you have a cookie, add it.
    //Create a www object, using form fields if you have them
    //if not, use no form fields but add cookies in headers 
    //if you have a cookie.
    public WWW CreateWWW(string url, PostDict pd, string cookiestr="")
    {
        WWWForm form = null;
        Dictionary<string, string> headers = null;
        WWW www;
        if (pd != null)
        {   
            Debug.Log("Had PostData");
            form =  new WWWForm();
            foreach (string key in pd.data.Keys)
            {
                form.AddField(key, pd.data[key]);
            }
            headers = new Dictionary<string, string>();
            foreach (string h in form.headers.Keys )
            {
                headers.Add(h, form.headers[h]);
            }
        }
        if (cookiestr != "")
        {
            Debug.Log("Had Cookie: " + cookiestr);
            //if (form != null)
            //{
            //    form.AddField("Cookie", cookiestr);
                //www = new WWW(url, form);
            //}
            //else
            //{
            //    headers = new Dictionary<string, string>();
                if (headers == null)
                    headers = new Dictionary<string, string>();
                headers.Add("Cookie", cookiestr);
                //www = new WWW(url, null, headers);
            //}
        }
        
        //Make the WWW obj
        if (form != null)
        {
            www = new WWW(url, form.data, headers);
        }
        else if (cookiestr != "")
        {
            www = new WWW(url, null, headers);
        }
        else
        {
            www = new WWW(url);
        }
        return www;
    }

    IEnumerator WaitForRequest(WWW www, HTTPCallback cb)
    {
        yield return www;
        //check for errors
        if (www.error == null)
        {
            //Debug.Log("HttpResponse: " + www.text);
            cb(www);
        }
        else
        {
            //Debug.Log("HttpError: " + www.error);
            DropletLog._instance.AddLog("HTTP Error", www.error);
            cb(www);
        }

    }
}
