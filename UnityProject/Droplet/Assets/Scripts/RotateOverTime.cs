﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour 
{
	public Vector3 rotationOverTime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(rotationOverTime * Time.deltaTime, Space.Self);	
	}
}