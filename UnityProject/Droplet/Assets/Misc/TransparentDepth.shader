﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Droplet/TransparentDepth" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
        _BumpMap ("Bumpmap", 2D) = "bump" {}
        _Cube ("Cubemap", CUBE) = "" {}


    }
    SubShader {
        Tags { "RenderType"="Transparent" "Queue"="Transparent+10"}
        LOD 200
        Zwrite On
        Blend SrcAlpha OneMinusSrcAlpha
        Pass {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
 
            #include "UnityCG.cginc"
           
            uniform sampler2D _MainTex;
            float4 _Color;
            float4 _BumpMap_ST;
            float4 _MainTex_ST;

            fixed4 frag(v2f_img i) : SV_Target {
                return tex2D(_MainTex, i.uv) * _Color;
            }
            ENDCG
        }

        
        CGPROGRAM
            // Physically based Standard lighting model, and enable shadows on all light types
            #pragma surface surf Standard fullforwardshadows

            // Use shader model 3.0 target, to get nicer looking lighting
            #pragma target 3.0

        
            struct Input {
                float2 uv_MainTex;
                float2 uv_BumpMap;
                float3 worldRefl;
                INTERNAL_DATA
            };
            
            fixed4 _Color;
            sampler2D _MainTex;
            sampler2D _BumpMap;
            half _Glossiness;
            half _Metallic;
            samplerCUBE _Cube;
            
            // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
            // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
            // #pragma instancing_options assumeuniformscaling
            UNITY_INSTANCING_BUFFER_START(Props)
                // put more per-instance properties here
            UNITY_INSTANCING_BUFFER_END(Props)

            void surf (Input IN, inout SurfaceOutputStandard o) {
                // Albedo comes from a texture tinted by color
                fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
                o.Albedo = c.rgb;
                o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
                // Metallic and smoothness come from slider variables
                o.Metallic = _Metallic;
                o.Smoothness = _Glossiness;
                o.Emission = texCUBE (_Cube, WorldReflectionVector (IN, o.Normal)).rgb;
                o.Alpha = c.a;
            }
            ENDCG
            
    }
    FallBack "Diffuse"
}
