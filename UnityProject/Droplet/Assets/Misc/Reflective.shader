﻿Shader "Droplet/Reflective" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
        _BumpMap ("Bumpmap", 2D) = "bump" {}
        _Cube ("Cubemap", CUBE) = "" {}
	}
	SubShader 
    {
        Pass
        {
            Tags { "RenderType"="Transparent" "Queue"="Transparent" }
            LOD 200
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM

                #pragma vertex Vert
                #pragma fragment Frag
                #include "UnityCG.cginc"

                float4 _Color;
                float4 _BumpMap_ST;
                float4 _MainTex_ST;

                struct Varyings
                {
                    float4 vertex : SV_POSITION;
                    float4 uvgrab : TEXCOORD0;
	                float2 uvbump : TEXCOORD1;
                	float2 uvmain : TEXCOORD2;
	                UNITY_FOG_COORDS(3)

                };

                Varyings Vert(appdata_base v)
                {
                    Varyings o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uvgrab = ComputeGrabScreenPos(o.vertex);
                	o.uvbump = TRANSFORM_TEX( v.texcoord, _BumpMap );
	                o.uvmain = TRANSFORM_TEX( v.texcoord, _MainTex );
                	UNITY_TRANSFER_FOG(o,o.vertex);

                    return o;
                }

                sampler2D _BumpMap;
                sampler2D _MainTex;

                float4 Frag(Varyings i) : SV_Target
                {
                    half4 tint = tex2D(_MainTex, i.uvmain);
                    half2 bump = UnpackNormal(tex2D( _BumpMap, i.uvbump )).rg; // we could optimize this by just reading the x & y without reconstructing the Z

                    return _Color * tint;
                }

            ENDCG
        }

        
        Pass
        {
            Tags { "LightMode"="ShadowCaster" "RenderType"="Opaque" "Queue"="Geometry" }

            CGPROGRAM

                #pragma vertex Vert
                #pragma fragment Frag
                #pragma multi_compile_shadowcaster
                #include "UnityCG.cginc"

                struct Varyings
                { 
                    V2F_SHADOW_CASTER;
                };

                Varyings Vert(appdata_base v)
                {
                    Varyings o;
                    TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                    return o;
                }

                float4 Frag(Varyings i) : SV_Target
                {
                    SHADOW_CASTER_FRAGMENT(i)
                }

            ENDCG
        }
        
        /*
        Pass
        {
            //Tags { "RenderType"="Opaque" }
            Tags { "RenderType"="Transparent" "Queue"="Transparent" }
            ZWrite Off
            LOD 200
            Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
                // Physically based Standard lighting model, and enable shadows on all light types
                #pragma surface surf Standard fullforwardshadows

                // Use shader model 3.0 target, to get nicer looking lighting
                #pragma target 3.0

            
                struct Input {
                    float2 uv_MainTex;
                    float2 uv_BumpMap;
                    float3 worldRefl;
                    INTERNAL_DATA
                };
                
                fixed4 _Color;
                sampler2D _MainTex;
                sampler2D _BumpMap;
                half _Glossiness;
                half _Metallic;
                samplerCUBE _Cube;
                
                // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
                // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
                // #pragma instancing_options assumeuniformscaling
                UNITY_INSTANCING_CBUFFER_START(Props)
                    // put more per-instance properties here
                UNITY_INSTANCING_CBUFFER_END

                void surf (Input IN, inout SurfaceOutputStandard o) {
                    // Albedo comes from a texture tinted by color
                    fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
                    o.Albedo = c.rgb;
                    o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
                    // Metallic and smoothness come from slider variables
                    o.Metallic = _Metallic;
                    o.Smoothness = _Glossiness;
                    o.Emission = texCUBE (_Cube, WorldReflectionVector (IN, o.Normal)).rgb;
                    o.Alpha = c.a;
                }
            ENDCG

         }
         */
	}
	FallBack "Diffuse"
}  
