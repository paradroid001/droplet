Code details
============
Bitbucket URL is https://bitbucket.org/paradroid001/droplet

Unity Version is 2018.1.1

Install support for Win/Android/WebGL at least

Installing on Windows:
1. Install Git (2.18.0 or above) and clone the repo
2. Install Python 2.7.11
3. Put python on the path (including Python27\Scripts): https://superuser.com/questions/143119/how-do-i-add-python-to-the-windows-path
4. pip install virtualenv
5. cd repo_dir/Server
6. virtualenv ve_droplet
7. ve_droplet\scripts\activate
8. pip install eggcache\django-1-11-4.tar.gz
9. cd droplet_server
8. python manage.py migrate
9. 
python manage.py loaddata --app suggestions 000_suggestion_settings.json
python manage.py loaddata --app suggestions 001_styles.json
python manage.py loaddata --app suggestions 002_categories.json
python manage.py loaddata --app suggestions 004_suggestioncategory.json
python manage.py loaddata --app suggestions 006_suggestions.json
10. deactivate

Now you can run startserver.bat (adjust root dir), startvis.bat, and update.bat




A Django Primer
==============

Django is a python library for creating web applications, and handles the server side logic and dealing with databases without writing SQL directly (therefore the DB type is mostly irrelevant).
Django 1.11.4 is used on this project.
Documentation: https://docs.djangoproject.com/en/1.11/

In terms of the MVC pattern, Django has:

1. M = the models defined in models.py of each ‘app’
1. V = the templates that render various pages
1. C = the view functions inside views.py. The naming is confusing, but technically you could do all your View and Controller inside views.py, the templates just bring the actual page rendering out into a separate set of assets, and leave your logic (the controller) inside views.py.

Project Structure
=================

* **manage.py** <— main project management console, gives you access to all the django commands.
* **droplet_server** <— contains the main app configuration, including settings.py
* **suggestions** <— ‘app’ (sub component of site) which has all the models/views/controllers for 
* **suggestions/models.py** holds all the classes that outline the data objects that end up going in the database
	*	**suggestions/views.py** holds the functions that are run when certain URLS are accessed. You can take different action based on whether it was a GET or POST request (AJAX requests are generally POST).
	*	**suggestions/urls.py** maps urls to view functions
	*	**suggestions/templates** holds the templates: html with extra django markup to allow pages to take variables and data passed to it when a view tries to render it.
	*	**suggestions/migrations** holds all the files which can transform the database schema (layout/organisation/table structure) to reflect the way the project looks at any point in time. As you change your project models and hence the database structure, the migrations directory is populated with files which will make these changes for you in the database so that it continues to reflect the models (for instance field name changes, or including new foreign key fields etc).
	*	**suggestion/fixtures** holds any data which can be used as initial imports for the database - for instance, default questions, categories, settings, profanity list, etc
*  **db.sqlite3** <— This is the SQLite database that is used during development. It is set to be ignored by git. So you should be able to delete it and initialise an entire site from scratch with manage.py, the migrations, and fixture data.
*  **static** <— assets here are always the same (like css files, images, js) to be served by the http server, usually via a URL to /static, i.e. http://127.0.0.1:8000/static/<whatever>
	*	**builds** <— The directory contents are ignored by GIT, so you can put WebGL builds of the Unity client here and access them via a static URL as above.

HTML Templating
===============

## jQuery - A Javascript library to make element organisation a bit easier.
Documentation: https://api.jquery.com/
Examples: https://www.w3schools.com/jquery/jquery_examples.asp

## Bootstrap - A set of CSS styles which save time
Documentation: https://v4-alpha.getbootstrap.com/getting-started/introduction/
Examples: https://v4-alpha.getbootstrap.com/examples/

Bootstrap and JQuery already sit in the static/css and static/js directories already.
The templates rendered by the Suggestion views are built on a hierarchy:
suggest_base.html <—JQuery/Bootstrap included here
	suggest_layout.html <—Custom CSS and basic page structure included here
		suggest.html <— The actual page that has the forms/buttons/etc


Manage.py examples
==================
To get started (after checking out the project, enabling your virtual environment, and changing to the Server/droplet_server directory)

	python manage.py migrate

This creates your database using the migration files within each app.

	python manage.py loaddata suggestions/fixtures/*

This installs some preexisting data which has been deliberately saved into .json files during development (say, defaults or starting data) to get your database populated with the bare minimum of data

	python manage.py createsuperuser
	
This creates a user who can access the site admin.

	python manage.py runserver
	
Runs the http server on a default of 127.0.0.1 port 8000 - you can specify alternates to run on different local IPs or ports. If this command isn’t running, your site won’t respond to browser requests. If you change some python code, the site automatically refreshes. If something is wrong, it will tell you.

## Other Commands
	python manage.py makemigrations

This checks to see if you’ve made any changes to models.py files, and creates migration files for you. You would then run manage.py migrate to apply the latest migrations to the database.


Data Transfer between server and client
=======================================
Data is transferred using JSON - javascript object notation, a text encoding which can transfer most basic types (int, float, string, bool, null, lists, dictionaries) as a large string of text, and supports nested structures. See: https://www.json.org/

## On the server side
Data is prepared by creating a dictionary with the fields we want (often implemented in a prepare()  method on the model), and then put into the HTTPResponse with a json.dumps() call.

## On the client side
The webpage content can interpret the JSON inside javascript functions as it wishes.
The unity client needs to create specific classes to deserialise the data, with the correct filenames (extra fields will just be ignored). For the most part these are defined in the DropletWebInterface.cs script, and should be marked with the System.Serializable decorator. An example is:

	[System.Serializable]
	public class Suggestion
	{
		public string text;
		public string category;
		public string enabled;
		public string style;
		public string suggestor;
		public string last_modified_date;
	}


This matches the server side creation of an object (which is then json serialised and sent via HTTP in response to the client request), for example in the prepare() method on the Suggestion model:

	def prepare(self):
			d = {}
			d["text"] = self.text;
			d["question"] = "%s" % self.question
			d["category"] = "%s" % self.question.category
			d["enabled"] = self.enabled
			d["style"] = self.style.fontstr() #.as_dict()
			d["suggestor"] = self.suggestor.__unicode__() #.as_dict()
			d["last_modified_date"] = str(self.last_modified_date)
			return d

TODO
====
 * graceful action when no web data?
 * limit on total orb pool from settings?
 * text file to tell client the URL
 * client side fonts?
 * proper thanks page
 * client side orbs make use of server side colours
